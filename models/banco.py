from models.cliente import Cliente
from models.conta import Conta
from models.transaction import Transaction

class Banco():
    def __init__(self, bankName: str):
        self.bankName = bankName
        self.contas = []
        self.clientes = []
        self.contadorConta = 1000

    def listarContas(self):
        print(self.name, '|', 'RELAÇÃO DE CONTAS DISPONÍVEIS')

        for conta in self.contas:
            conta.printInfo()

        print('Listagem finalizada')

    def debitarConta(self, numeroDaConta, valor):
        for conta in self.contas:
            if conta.numeroDaConta == numeroDaConta:
                try:
                    conta.debitar(valor)
                    print('Conta', numeroDaConta, 'debitada')
                    print('Ńovo saldo:', conta.saldo, '\n', '-' * 10)
                except:
                    raise Exception('Não foi possível realizar o débito da conta',
                          numeroDaConta)


    def creditarConta(self, numeroDaConta, valor):
        for conta in self.contas:
            if conta.numeroDaConta == numeroDaConta:
                conta.creditar(float(valor))
                print('Conta', numeroDaConta, 'creditada, valor R$', valor)
                print('Saldo: R$', conta.saldo, '\n' + '-' * 10)
                return

        print('Conta não encontrada\n', '-' * 10)

    def consultarSaldo(self, numeroDaConta: int):
        for conta in self.contas:
            if conta.numeroDaConta == numeroDaConta:
                conta.printInfo()
                print('Conta', numeroDaConta, '\nSaldo:',
                      conta.saldo, '\n', '-' * 10)
                return

        print('Conta não encontrada')

    def abrirContaCorrente(self, nomeCliente: str, cpfCliente: str, idadeCliente: int):
        self._abrirConta(nomeCliente, cpfCliente, idadeCliente, 0)

    def abrirContaPoupanca(self, nomeCliente: str, cpfCliente: str, idadeCliente: int):
        self._abrirConta(nomeCliente, cpfCliente, idadeCliente, 1)

    def _abrirConta(self, nomeCliente: str, cpfCliente: str, idadeCliente: int, tipoConta):
        cliente = Cliente(nomeCliente, cpfCliente, idadeCliente)
        self.clientes.append(cliente)

        conta = Conta(tipoConta, cliente, self.contadorConta)
        self.contas.append(conta)

        print('\n\x1b[92mConta número', self.contadorConta, 'aberta com sucesso.\x1b[0m\n')
        self.contadorConta = self.contadorConta + 1

    def emitirExtrato(self, numeroDaConta):
        for conta in self.contas:
            if conta.numeroDaConta == numeroDaConta:
                print('Extrato - Conta número', numeroDaConta)
                print('Data   -   Tipo   -   Valor')
                conta.listarMovimentacoes()
                print('Extrato finalizado\n', '-' * 10)
                return
        print('Conta', numeroDaConta, 'não encontrada.')


if __name__ == "__main__":

    from cliente import Cliente
    from conta import Conta

    banco = Banco('Banco teste')

    banco.abrirContaCorrente('Daniel Dutra', '00099988877', 37)
    banco.abrirContaPoupanca('Daniel Dutra', '00099988877', 37)
    banco.consultarSaldo(10000)
    banco.consultarSaldo(1000)
    banco.creditarConta(5, 50)
    banco.creditarConta(1000, 50)
    banco.debitarConta(1000, 25)
    banco.debitarConta(1000, 50)

    banco.emitirExtrato(1000)
