from datetime import date

TIPOS_DE_TRANSACAO = (
    (0, 'Depósito'),
    (1, 'Saque'),
)

class Transaction:

    def __init__(self, valor, tipo: TIPOS_DE_TRANSACAO):
        self.valor = valor
        self.tipo = tipo
        self.data = date.today()

    def printDetail(self):
        print(self.data,'|',self.tipo,'| R$',self.valor)

if __name__ == '__main__':

    t1 = Transaction(50,0)
    t2 = Transaction(-50,0)

    print('Valor:',t1.valor,'Tipo:',t1.tipo,'Data:',t1.data)
    t1.printDetail()
    t2.printDetail()