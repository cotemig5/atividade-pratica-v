from models.transaction import Transaction

TIPOS_DE_CONTA = (
    (0, 'Corrente'),
    (1, 'Poupança'),
)
class Conta:
    def __init__(self, tipo_de_conta: TIPOS_DE_CONTA, cliente, numeroDaConta: int):
        self.tipo_de_conta = tipo_de_conta
        self.cliente = cliente
        self.saldo = 0
        self.movimentacoes = []
        self.numeroDaConta = numeroDaConta

    def printInfo(self):
        print('Tipo de conta:',self.tipo_de_conta)
        print('Titular:',self.cliente.nome)
        print('Número da conta:',self.numeroDaConta)
        print('CPF:',self.cliente.cpf)
        print('Idade:',self.cliente.idade)
        print('-' * 10)

    def debitar(self, valor):
        if abs(valor) > self.saldo:
            raise Exception('Saldo insuficiente')

        else:
            self.saldo = self.saldo - abs(valor)
            t = Transaction(abs(valor) * -1,1)
            self.movimentacoes.append(t)
    
    def creditar(self, valor):
        self.saldo = self.saldo + abs(valor)

        t = Transaction(abs(valor),0)


    def listarMovimentacoes(self):
        for t in self.movimentacoes:
            t.printDetail()
        


if __name__ == '__main__':
    from cliente import Cliente
    from transaction import Transaction


    cliente = Cliente('Daniel','0099988877',37)

    conta = Conta(0,cliente,0)
    conta.printInfo()
    conta.creditar(50)
    conta.creditar(-50)
    conta.debitar(-10)
    conta.debitar(1)
    conta.listarMovimentacoes()
    