class Menus:
    active = True

    def __init__(self, banco):
        self.banco = banco

    def separador(self):
        print('-' * 30)

    def menuInicial(self):
        opcao = None
        self.separador()
        print('Bem vindo ao', self.banco.bankName)
        self.separador()
        while True:
            print('Escolha a opção desejada:')
            print('1 - Consulta a saldo')
            print('2 - Depósito')
            print('3 - Saque')
            print('4 - Abertura de conta')
            print('0 - Sair')
            opcao = input()

            try:
                if opcao == '0':
                    self.sair()

                if opcao == '1':
                    self.consultarSaldo()
                if opcao == '2':
                    self.depositar()
                if opcao == '3':
                    self.sacar()
                if opcao == '4':
                    self.abrirConta()
                raise Exception
            except:
                print('Opção inválida')

    def sacar(self):
        numeroDaConta = None
        valor = None

        self.separador()
        print('Saque em conta corrente ou poupança')
        self.separador()

        while True:
            try:
                print('Informe o número da conta:')
                numeroDaConta = int(input())
                print('Informe o valor do saque:')
                valor = float(input())
                self.banco.debitarConta(numeroDaConta, valor)
                self.menuInicial()

            except Exception as e:
                print('Houve um erro', e)
                self.menuInicial()

    def abrirConta(self):
        tipoDeConta = None
        nome = None
        cpf = None
        idade = None

        self.separador()
        print('Abertura de conta (poupança ou corrente')
        self.separador()

        while True:
            try:
                print(
                    'Informe o tipo de conta\n(0 - conta corrente, 1 - conta poupança)')
                tipoDeConta = int(input())
                print('Informe o nome completo do cliente:')
                nome = input()
                print('Informe o CPF:')
                cpf = input()
                print('Informe a idade do cliente')
                idade = input()

                if tipoDeConta == 0:
                    self.banco.abrirContaCorrente(nome, cpf, idade)

                if tipoDeConta == 1:
                    self.banco.abrirContaCorrente(nome, cpf, idade)

                self.menuInicial()

            except Exception as e:
                print('Não foi possível abrir a conta', e)
                self.menuInicial()

    def depositar(self):
        numeroDaConta = None
        valor = None

        self.separador()
        print('Realizar depósito')
        self.separador()

        while True:
            try:
                print('Informe o número da conta ou 0 (zero) para sair:')
                numeroDaConta = input()
                if numeroDaConta == '0':
                    self.menuInicial()
                print('Informe o valor (use ponto para indicar a vírgula):')
                valor = input()

                self.banco.creditarConta(int(numeroDaConta), valor)

            except Exception as e:
                print('Houve um erro', e)
                self.menuInicial()

    def consultarSaldo(self):
        entrada = None

        self.separador()
        print('Consultar saldo')
        self.separador()

        while True:
            print('Digite o número da conta ou 0 (zero) para voltar ao menu inicial:')
            entrada = input()

            try:
                if entrada == '0':
                    self.menuInicial()
                self.banco.consultarSaldo(int(entrada))
            except Exception as e:
                print('Opção inválida', e)

    def sair(self):
        import os

        print('-' * 60)
        print('Obrigado por utilizar os serviços do Banco Cotemig!')
        print('-' * 60)
        return os._exit(0)

